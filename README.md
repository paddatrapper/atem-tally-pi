# Raspberry Pi ATEM Tally Lights

3D printed ATEM tally light using a Raspberry Pi.

## Hardware

See the hardware description in the [hardware directory](hardware).

## Software

The tally light runs the Python script under [atemtally](atemtally). You can run
the script using

```
virtualenv -p python3 pyenv
pyenv/bin/pip install -e .
```
