#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import time
import PyATEMMax

MIX_EFFECT = 0

def run(switcher_address, camera):
    switcher = PyATEMMax.ATEMMax()
    switcher.connect(switcher_address)
    switcher.waitForConnection()
    last_src = switcher.programInput[MIX_EFFECT].videoSource
    while True:
        src = switcher.programInput[MIX_EFFECT].videoSource
        if src != last_src:
            if src == camera:
                print(f'Camera ON')
            if last_src == camera:
                print(f'Camra OFF')
            last_src = src
        time.sleep(0.01)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='ATEM tally light')
    parser.add_argument('-s', '--switcher', help='the address of the ATEM Switcher')
    parser.add_argument('-n', '--camera', help='the source number to monitor')
    args = parser.parse_args()
    run(args.switcher, args.camera)
