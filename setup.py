from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='atem-tally',
    description='ATEM tally lights running on a Raspberry Pi',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/bitcast/mumbleice',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Other Audience',
        'Topic :: Multimedia :: Video',
        'Topic :: System :: Hardware',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Environment :: Console',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
    keywords='atem raspberry pi tally light',
    packages=find_packages(),
    install_requires=[
        'gpiozero',
        'PyATEMMax',
        'pytest',
    ],
    entry_points={
        'console_scripts': ['atemtally=atemtally.__main__:run'],
    },
    package_data={
        'atemtally': [
            'LICENCE.md',
        ],
    },
)

